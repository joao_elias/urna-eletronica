/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.poo.urna.model;

/**
 *
 * @author João
 */
public class Candidato {

    private String nome;
    private String numero;
    private Partido partido; 
    private int voto;

    public Candidato() {

    }

    @Override
    public String toString() {
        return this.getNome() + ", Número: " + this.getNumero()+"\n";
    }

    public Candidato(String numero, String nome) {
        this.nome = nome;

    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return the partido
     */
    public Partido getPartido() {
        return partido;
    }

    /**
     * @param partido the partido to set
     */
    public void setPartido(Partido partido) {
        this.partido = partido;
    }

    /**
     * @return the votos
     */
    public int getVoto() {
        return voto;
    }

    /**
     * @param voto
     */
    public void setVoto(int voto) {
        if (voto == 1) {
            this.voto++;
        }
    }

}
