/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.poo.urna.service;

import br.com.poo.urna.exception.BusinessException;
import br.com.poo.urna.model.Candidato;
import br.com.poo.urna.model.Partido;
import br.com.poo.urna.persistence.UrnaDAO;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author João
 */
public class UrnaService {

    public static List<Candidato> listaDeCandidatos = null;
   

    public UrnaService() throws BusinessException {
        try {
            listaDeCandidatos = UrnaDAO.pesquisarListaCandidato();
        } catch (IOException ex) {
            throw new BusinessException("Erro inesperado!");
        }

    }

    public Candidato pesquisarListaCandidato(String numero) throws BusinessException {
        Candidato candidato = null;
        for (Candidato c : listaDeCandidatos) {

            if (numero.equals(c.getNumero())) {
                candidato = c;
                break;

            }
        }

        if (candidato == null) {
            throw new BusinessException("NUMERO ERRADO");
        }

        return candidato;
    }

    public Partido pesquisarPartido(String numero) throws BusinessException {
        Partido partido = null;
        try {
            partido = UrnaDAO.pesquisarPartido(numero);
            if (partido == null) {
                throw new BusinessException("NUMERO ERRADO");
            }
        } catch (IOException ex) {
            Logger.getLogger(UrnaService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return partido;
    }

    public Candidato votar(String numero) {
        for (Candidato candidato : listaDeCandidatos) {
            if (numero.equals(candidato.getNumero())) {
                candidato.setVoto(1);
                return candidato;
            }

        }
        return null;
    }
}
