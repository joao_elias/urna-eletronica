/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.poo.urna.presentation;

import br.com.poo.urna.exception.BusinessException;
import br.com.poo.urna.model.Candidato;
import br.com.poo.urna.model.Partido;
import br.com.poo.urna.service.UrnaService;
import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author João
 */
public class UrnaJFrame extends javax.swing.JFrame {

    public boolean passo;
    public float totVot = 0;
    public float votNul=0;
    public float votBra = 0;
    public String[] vct = new String[4];
    public boolean possivel = false;
    private UrnaService service = null;
    DecimalFormat df = new DecimalFormat("##.##");
    /**
     * Creates new form UrnaJFrame
     */
    public UrnaJFrame() {
        initComponents();
        labelImage.setIcon(null);
        try {
            service = new UrnaService();
        } catch (BusinessException ex) {
            Logger.getLogger(UrnaJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void Contagem() {
        labelErro.setText("");  
        JOptionPane.showMessageDialog(null, imprimirResultado());
    }

    private void preencherCampos(String numero) {
        if (textNum1.getText().equals("")) {
            textNum1.setText(numero);
            vct[0] = numero;
        } else if (textNum2.getText().equals("")) {
            textNum2.setText(numero);
            vct[1] = numero;
        } else if (textNum3.getText().equals("")) {
            textNum3.setText(numero);
            vct[2] = numero;
        } else if (textNum4.getText().equals("")) {
            textNum4.setText(numero);
            vct[3] = numero;
        }
    }

    private boolean verificaCampos() {
        return textNum1.getText().equals("") && labelPartido.getText().equals(""); //valor vazio retorna verdadeiro
    }

 
    public String imprimirResultado() {

        totVot = totVot / 100;
        
        StringBuilder result = new StringBuilder();
 
        List<Candidato> lista = service.listaDeCandidatos;
        
        result.append("Fim da votação.\nTotal: ").append(df.format(totVot * 100)).append("\n\nNulos: ").append(df.format(votNul / totVot)).append("% (").append(df.format(votNul)).append(")").append("\nVotos brancos: ").append(df.format(votBra / totVot)).append("% (").append(df.format(votBra)).append(")").append("\nVotos: \n");
       
        
        for (Candidato candidato : lista) {
            result.append(candidato.getNome());
            result.append(" = ");
            result.append(df.format(candidato.getVoto()/totVot));
            result.append("% \t (");
            result.append(candidato.getVoto());
             result.append(" votos)");
            result.append("\n"); 
        }      
          
        
        return result.toString();

    }

    private void comparar() { //comparar o candidato
        try {

            String numero = (textNum1.getText() + "" + textNum2.getText() + "" + textNum3.getText() + "" + textNum4.getText());
            Candidato candidato = service.pesquisarListaCandidato(numero);

            labelNome.setText(candidato.getNome());
            labelPartido.setText(candidato.getPartido().getSigla());
            labelImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/" + candidato.getNumero() + ".jpg")));
        } catch (BusinessException ex) {
            labelErro.setText(ex.getMessage());

        }
    }

    private void comparar2() { //comparar o partido
        try {

            String numero = (textNum1.getText() + "" + textNum2.getText() + "" + textNum3.getText() + "" + textNum4.getText());
            Partido partido = service.pesquisarPartido(numero);
            labelPartido.setText(partido.getSigla());
            labelPartidoTexto.setText("Partido: ");
        } catch (BusinessException ex) {
            labelErro.setText(ex.getMessage());
        }
    }

    private void compararParaVotar() {
        String numeroInformado = vct[0] + vct[1] + vct[2] + vct[3];
        if ("9999".equals(numeroInformado)) { // n�mero m�gico
            totVot--;
            Contagem();
            dispose();
        } else if (textNum1.getText().equals("")) {
            votBra++;
        } else {
            this.votar(numeroInformado);
        }
    }

    private void votar(String numero) {
        if(service.votar(numero)==null){
            votNul++;
        }
    }

    private void chamaAudio() {
        URL url = getClass().getResource("/sound/end.wav");
        AudioClip audio = Applet.newAudioClip(url);
        audio.play();
    }

    private void chamaAudioErro() {
        URL url = getClass().getResource("/sound/ops.wav");
        AudioClip audio = Applet.newAudioClip(url);
        audio.play();
    }

    private void chamarAudioVotoFinalizado() {
        URL url = getClass().getResource("/sound/inter.wav");
        AudioClip audio = Applet.newAudioClip(url);
        audio.play();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelCand = new javax.swing.JPanel();
        textNum1 = new javax.swing.JTextField();
        labelTitulo = new javax.swing.JLabel();
        textNum2 = new javax.swing.JTextField();
        textNum3 = new javax.swing.JTextField();
        textNum4 = new javax.swing.JTextField();
        labelErro = new javax.swing.JLabel();
        labelImage = new javax.swing.JLabel();
        labelNome = new javax.swing.JLabel();
        labelPartidoTexto = new javax.swing.JLabel();
        labelPartido = new javax.swing.JLabel();
        btAjuda = new javax.swing.JButton();
        panelNumerico = new javax.swing.JPanel();
        bt1 = new javax.swing.JButton();
        bt2 = new javax.swing.JButton();
        bt3 = new javax.swing.JButton();
        bt4 = new javax.swing.JButton();
        bt5 = new javax.swing.JButton();
        bt6 = new javax.swing.JButton();
        bt7 = new javax.swing.JButton();
        bt8 = new javax.swing.JButton();
        bt9 = new javax.swing.JButton();
        bt0 = new javax.swing.JButton();
        btBRA = new javax.swing.JButton();
        btCOR = new javax.swing.JButton();
        btCOF = new javax.swing.JButton();
        labelJustica = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Simulador de votação");

        panelCand.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        textNum1.setFont(new java.awt.Font("Arial", 0, 19)); // NOI18N
        textNum1.setEnabled(false);
        textNum1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textNum1ActionPerformed(evt);
            }
        });

        labelTitulo.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        labelTitulo.setText("Deputado Federal");

        textNum2.setFont(new java.awt.Font("Arial", 0, 19)); // NOI18N
        textNum2.setEnabled(false);
        textNum2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textNum2ActionPerformed(evt);
            }
        });

        textNum3.setFont(new java.awt.Font("Arial", 0, 19)); // NOI18N
        textNum3.setEnabled(false);
        textNum3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textNum3ActionPerformed(evt);
            }
        });

        textNum4.setFont(new java.awt.Font("Arial", 0, 19)); // NOI18N
        textNum4.setEnabled(false);
        textNum4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textNum4ActionPerformed(evt);
            }
        });

        labelErro.setFont(new java.awt.Font("Arial", 0, 15)); // NOI18N

        labelImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/9101.jpg"))); // NOI18N

        labelNome.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        labelPartidoTexto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelPartidoTexto.setText(" ");

        labelPartido.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        btAjuda.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 14)); // NOI18N
        btAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/imageres_99-7.png"))); // NOI18N
        btAjuda.setText("Ajuda");
        btAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAjudaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCandLayout = new javax.swing.GroupLayout(panelCand);
        panelCand.setLayout(panelCandLayout);
        panelCandLayout.setHorizontalGroup(
            panelCandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCandLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCandLayout.createSequentialGroup()
                        .addGroup(panelCandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelErro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(labelTitulo)
                            .addGroup(panelCandLayout.createSequentialGroup()
                                .addComponent(textNum1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(textNum2, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(textNum3, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(textNum4, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(panelCandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelCandLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btAjuda))
                            .addGroup(panelCandLayout.createSequentialGroup()
                                .addGap(152, 152, 152)
                                .addGroup(panelCandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelNome, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(labelImage))
                                .addGap(0, 13, Short.MAX_VALUE))))
                    .addGroup(panelCandLayout.createSequentialGroup()
                        .addGroup(panelCandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelPartido, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelPartidoTexto, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelCandLayout.setVerticalGroup(
            panelCandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCandLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(panelCandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCandLayout.createSequentialGroup()
                        .addComponent(labelTitulo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelCandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(textNum1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textNum2, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textNum3, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textNum4, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(labelImage))
                .addGap(18, 18, 18)
                .addComponent(labelNome, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(labelPartidoTexto, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelPartido, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(panelCandLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCandLayout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(btAjuda))
                    .addGroup(panelCandLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(labelErro, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelNumerico.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        bt1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/n1.jpg"))); // NOI18N
        bt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt1ActionPerformed(evt);
            }
        });

        bt2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/n2.jpg"))); // NOI18N
        bt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt2ActionPerformed(evt);
            }
        });

        bt3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/n3.jpg"))); // NOI18N
        bt3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt3ActionPerformed(evt);
            }
        });

        bt4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/n4.jpg"))); // NOI18N
        bt4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt4ActionPerformed(evt);
            }
        });

        bt5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/n5.jpg"))); // NOI18N
        bt5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt5ActionPerformed(evt);
            }
        });

        bt6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/n6.jpg"))); // NOI18N
        bt6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt6ActionPerformed(evt);
            }
        });

        bt7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/n7.jpg"))); // NOI18N
        bt7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt7ActionPerformed(evt);
            }
        });

        bt8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/n8.jpg"))); // NOI18N
        bt8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt8ActionPerformed(evt);
            }
        });

        bt9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/n9.jpg"))); // NOI18N
        bt9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt9ActionPerformed(evt);
            }
        });

        bt0.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/n0.jpg"))); // NOI18N
        bt0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt0ActionPerformed(evt);
            }
        });

        btBRA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/branco.jpg"))); // NOI18N
        btBRA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBRAActionPerformed(evt);
            }
        });

        btCOR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/corrige.jpg"))); // NOI18N
        btCOR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCORActionPerformed(evt);
            }
        });

        btCOF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/confirma.jpg"))); // NOI18N
        btCOF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCOFActionPerformed(evt);
            }
        });

        labelJustica.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/justica.jpg"))); // NOI18N

        javax.swing.GroupLayout panelNumericoLayout = new javax.swing.GroupLayout(panelNumerico);
        panelNumerico.setLayout(panelNumericoLayout);
        panelNumericoLayout.setHorizontalGroup(
            panelNumericoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNumericoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelNumericoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelJustica)
                    .addGroup(panelNumericoLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(panelNumericoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelNumericoLayout.createSequentialGroup()
                                .addComponent(bt4, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt5, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt6, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelNumericoLayout.createSequentialGroup()
                                .addComponent(bt1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt2, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelNumericoLayout.createSequentialGroup()
                                .addGroup(panelNumericoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btBRA, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(bt7, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelNumericoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(bt0, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(panelNumericoLayout.createSequentialGroup()
                                        .addComponent(bt8, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(bt9, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelNumericoLayout.createSequentialGroup()
                                        .addComponent(btCOR, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btCOF, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelNumericoLayout.setVerticalGroup(
            panelNumericoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNumericoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelJustica)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelNumericoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt2, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt3, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelNumericoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt4, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt6, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelNumericoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt7, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt8, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt9, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bt0, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(panelNumericoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btBRA, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btCOR, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btCOF, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelCand, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(panelNumerico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(panelCand, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelNumerico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void textNum1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textNum1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textNum1ActionPerformed

    private void textNum2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textNum2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textNum2ActionPerformed

    private void textNum3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textNum3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textNum3ActionPerformed

    private void textNum4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textNum4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textNum4ActionPerformed

    /*
     * M�todo refatorado para reaproveitar o c�digo e melhorar a manutenibilidade, se precisar alterar algo voc� edita em um �nico local
     *
     **/
    private void buscarPartido() {
        if (!textNum1.getText().equals("") && !textNum2.getText().equals("")) {
            comparar2();
            if (!textNum3.getText().equals("") && !textNum4.getText().equals("")) {
                comparar();
            }
        }
    }

    private void btBRAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBRAActionPerformed
        passo = true;
        if (verificaCampos()) {
            labelPartido.setText("VOTO BRANCO");
        } else {
            chamaAudioErro();
            JOptionPane.showMessageDialog(null, "<html>Para votar em <strong>BRANCO</strong> <br /> o campo de voto deve estar vazio.<br /> Aperte CORRIGE para apagar o campo de voto.<br /><br /></html>", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btBRAActionPerformed

    private void bt0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt0ActionPerformed
        // TODO add your handling code here:
        preencherCampos("0");
        // Refatorado
        buscarPartido();
    }//GEN-LAST:event_bt0ActionPerformed

    private void bt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt1ActionPerformed
        // TODO add your handling code here:
        preencherCampos("1");
        // Refatorado
        buscarPartido();
    }//GEN-LAST:event_bt1ActionPerformed

    private void bt2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt2ActionPerformed
        // TODO add your handling code here:
        preencherCampos("2");
        // Refatorado
        buscarPartido();
    }//GEN-LAST:event_bt2ActionPerformed

    private void bt3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt3ActionPerformed
        // TODO add your handling code here:
        preencherCampos("3");
        // Refatorado
        buscarPartido();
    }//GEN-LAST:event_bt3ActionPerformed

    private void bt4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt4ActionPerformed
        // TODO add your handling code here:
        preencherCampos("4");
        // Refatorado
        buscarPartido();
    }//GEN-LAST:event_bt4ActionPerformed

    private void bt5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt5ActionPerformed
        // TODO add your handling code here:
        preencherCampos("5");
        // Refatorado
        buscarPartido();
    }//GEN-LAST:event_bt5ActionPerformed

    private void bt6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt6ActionPerformed
        // TODO add your handling code here:
        preencherCampos("6");
        // Refatorado
        buscarPartido();
    }//GEN-LAST:event_bt6ActionPerformed

    private void bt7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt7ActionPerformed
        // TODO add your handling code here:
        preencherCampos("7");
        // Refatorado
        buscarPartido();
    }//GEN-LAST:event_bt7ActionPerformed

    private void bt8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt8ActionPerformed
        // TODO add your handling code here:
        preencherCampos("8");
        // Refatorado
        buscarPartido();
    }//GEN-LAST:event_bt8ActionPerformed

    private void bt9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt9ActionPerformed
        // TODO add your handling code here:
        preencherCampos("9");
        // Refatorado
        buscarPartido();
    }//GEN-LAST:event_bt9ActionPerformed

    private void btCORActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCORActionPerformed
        // TODO add your handling code here:
        if (verificaCampos()) {
            chamaAudioErro();
            JOptionPane.showMessageDialog(null, "<html>Para utilizar o <strong>CORRIGE</strong> <br /> você deve ter digitado algum número<br /> ou ter votado em BRANCO. <br /><br /></html>", "Alerta", JOptionPane.WARNING_MESSAGE);
        } else {
            textNum1.setText("");
            textNum2.setText("");
            textNum3.setText("");
            textNum4.setText("");
            labelImage.setIcon(null);
            labelErro.setText("");
            labelNome.setText("");
            labelPartido.setText("");
            labelPartidoTexto.setText("");
            passo = false;
        }
    }//GEN-LAST:event_btCORActionPerformed

    private void btCOFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCOFActionPerformed
        possivel = true;
        passo = true;
        this.totVot++;
        compararParaVotar(); 
        if(!textNum1.getText().equals("") || !labelPartido.getText().equals("")){
        btCORActionPerformed(evt); 
        }
        chamarAudioVotoFinalizado(); 
        JOptionPane.showMessageDialog(null, "Voto computado com sucesso!", "Sucesso", JOptionPane.PLAIN_MESSAGE);
        chamaAudio();
        JOptionPane.showMessageDialog(null, "FIM");
        passo = true;
    }//GEN-LAST:event_btCOFActionPerformed

    private void btAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAjudaActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Candidatos disponíveis:\n\n"+UrnaService.listaDeCandidatos);
    }//GEN-LAST:event_btAjudaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
        
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UrnaJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UrnaJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UrnaJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UrnaJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UrnaJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bt0;
    private javax.swing.JButton bt1;
    private javax.swing.JButton bt2;
    private javax.swing.JButton bt3;
    private javax.swing.JButton bt4;
    private javax.swing.JButton bt5;
    private javax.swing.JButton bt6;
    private javax.swing.JButton bt7;
    private javax.swing.JButton bt8;
    private javax.swing.JButton bt9;
    private javax.swing.JButton btAjuda;
    private javax.swing.JButton btBRA;
    private javax.swing.JButton btCOF;
    private javax.swing.JButton btCOR;
    private javax.swing.JLabel labelErro;
    private javax.swing.JLabel labelImage;
    private javax.swing.JLabel labelJustica;
    private javax.swing.JLabel labelNome;
    private javax.swing.JLabel labelPartido;
    private javax.swing.JLabel labelPartidoTexto;
    private javax.swing.JLabel labelTitulo;
    private javax.swing.JPanel panelCand;
    private javax.swing.JPanel panelNumerico;
    private javax.swing.JTextField textNum1;
    private javax.swing.JTextField textNum2;
    private javax.swing.JTextField textNum3;
    private javax.swing.JTextField textNum4;
    // End of variables declaration//GEN-END:variables
}
