/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.poo.urna.persistence;

import br.com.poo.urna.model.Candidato;
import br.com.poo.urna.model.Partido;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  
 * @author João Elias Ferraz & Crislon Crespo Jesus
 */
public class UrnaDAO { 

    public static List<Candidato> pesquisarListaCandidato() throws IOException {
        FileReader arq = null;
        List<Candidato> lista = new ArrayList<>();
        try {
            arq = new FileReader(System.getProperty("user.dir") + "\\src\\resources\\database.txt");
            BufferedReader lerArq = new BufferedReader(arq);
            String linha = lerArq.readLine();

            while (linha != null) {
                String dados[] = linha.split(";");
                Candidato candidato = new Candidato();
                candidato.setNome(dados[4]);
                candidato.setNumero(dados[3]);
                Partido partido = new Partido();
                partido.setNome(dados[2]);
                partido.setNumero(Integer.parseInt(dados[0]));
                partido.setSigla(dados[1]);
                candidato.setPartido(partido);
                lista.add(candidato);

                linha = lerArq.readLine();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UrnaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UrnaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            arq.close();
        }
        return lista;
    }

    public static Partido pesquisarPartido(String numero) throws IOException {
        Partido partido = null;
        FileReader arq = null;

        try {
            arq = new FileReader(System.getProperty("user.dir") + "\\src\\resources\\database.txt");
            BufferedReader lerArq = new BufferedReader(arq);
            String linha = lerArq.readLine();
            while (linha != null) {
                if (linha.contains("" + numero)) {
                    String infor[] = linha.split(";");
                    partido = new Partido();

                    partido.setSigla(infor[1]);

                }

                linha = lerArq.readLine();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UrnaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UrnaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            arq.close();
        }
        return partido;
    }

}
